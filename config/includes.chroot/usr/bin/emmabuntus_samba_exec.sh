#!/bin/bash

# Emmabuntus_samba_exec.sh --
#
#   This file informe about limitation
#   of Samba for Emmabuntüs distribution.
#
#   Created on 2010-22 by collectif Emmabuntüs (contact@emmabuntus.org).
#
#   It was validate on Debian 12 XFCE/LXQt.
#
#   Home web site : https://emmabuntus.org/
#
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


################################################################################

clear

config_samba_file=/etc/samba/smb.conf

# Récupération de l'argument après le passage en root
SHARE_NOM=$1
AURORISE_ECRITURE=$2


if  [ ${AURORISE_ECRITURE} == "false" ] ; then
COMMENT_ECRITURE=";"
else
COMMENT_ECRITURE=""
fi


if [[ $LANG == fr* ]] ; then
COMMENT="Partage Samba en mode invité"
else
COMMENT="Samba sharing in guest mode"
fi


config_samba="\n\
[${SHARE_NOM}]\n\
\tcomment = ${COMMENT}\n\
\tpath = /home/${SHARE_NOM}\n\
${COMMENT_ECRITURE}\twritable = yes\n\
\tguest ok = yes\n\
\tcreate mode = 0777\n\
\tdirectory mode = 0777"

# Creation du dossier
sudo mkdir -p /home/${SHARE_NOM}
sudo chmod 777 /home/${SHARE_NOM}

# configuration de Samba
echo -e ${config_samba} | sudo tee -a ${config_samba_file}

# relance du serveur Samba
sudo systemctl restart smbd nmbd

exit 0

